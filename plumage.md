# Le plumage

Le plumage, en grammaire ikienne, décrit sous plusieurs angles, le comportement, l'usage et le contenu du lexique d'une langue. Il convient de rappeler que la notion de "lexique" en grammaire ikienne est assez étendues et comprendra également des phrases, par exemple. Pour cette même raison, on préfèrera l'usage de "terme" à celui de "mot".

Lorsque l'on parle du comportement du lexique, cela comprendra également plusieurs éléments tels que le niveau des termes qui le compose, l'étymologie, les transformations, les connotations, etc.

Il est donc assez difficile de faire une description complète du plumage, mais on va essayer.

# L'identité

L'identité d'un terme est ce qui permet de l'indentifier par rapport à tout autre terme. L'identité est composée de quatre éléments :

 * `Le terme` : Le terme en lui-même. (e.g. manger, maison, boire)
 * `Le rôle` : Le rôle du terme. (e.g. verbe, nom, adjectif)
 * `Les catégories` : Les différents catégories du terme. (e.g. féminin, animé, transitif)
 * `Le niveau` : Le niveau du terme. (e.g. 1, 2, 3, 4)

> On notera que pour faciliter la compréhension, j'ai mis des exemples parlant pour un français. En Deyryck, nous aurions eus des exemples comme : "objectif, thème" pour le rôle et "mot adaran, filien, tolérant" pour les catégories.

> Petite dérive : Le Deyryck ne possède presque aucun rôle parce que le rôle, contrairement à une catégorie, change l'usage du terme dans l'expression. En Deyryck, par exemple, même un synthétiseur sera considéré comme un thème, là où en français on fera presque systématiquement une distinction de rôle.

# Le niveau

Dans l'identité d'un mot, nous avons parlé de "niveau". Le niveau d'un mot décrit son niveau de précision.

On note généralement les niveaux suivants :

 * **Le niveau 0** : Nuance/Primaire
 * **Le niveau 1** : Idée
 * **Le niveau 2** : Domaine
 * **Le niveau 3** : Notions
 * **Le niveau 4** : Précision
 * **Le niveau 5** : Spécificité
 * **Le niveau 6** : Image/Référence
 * **Le niveau 7** : Information exacte
 * **Le niveau 8** : Jargon
 * **Le niveau 9** : Explicitation/Information totale
 * **Le niveau 10** : Définition

Cela dit, on peut totalement imaginer des niveaux supplémentaires.