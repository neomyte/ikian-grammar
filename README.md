# Grammaire Ikienne

Présentation de la grammaire vu par des ikiens.

# Types de langues

La grammaire ikienne divise les langues en 4 types :

 * **Petite** (molgas'lizam) : Une langue peu aboutie, oubliée, morte.
 * **Divine** (molgas'maatéyam) : Une langue dont la complexité est très/trop importante et qui se décompose souvent en plusieurs sous langues.
 * **Déyam** : Une langue qui se focalise sur l'expression de l'intention. Généralement prévue majoritairement pour la communication orale.
 * **Kjiam** : Une langue qui se focalise sur l'expression de l'information. Généralement prévue majoritairement pour la communication écrite.

> Ainsi, le français est considéré comme un Kjiam là où le Deyryck est considéré comme un Déyam.

# Ecriture et phonologie

A ajouter plus tard. Cela étant dit, au sens stricte, ni l'un ni l'autre ne sont compris dans la "grammaire".

# La syntaxe

La syntaxe d'une langue toujours séparée en deux parties, les oiseaux et la synthèse.

On notera qu'en grammaire ikienne, la syntaxe décrit non pas une phrase mais une **expression**.

Dans la grammaire ikienne, la phrase est une unité de plumage, un groupement de mot qui forme un élément lexicale de plus grande envergure.

L'expression, quant à elle, décrit un élément du discours qui exprime un fait et/ou une intention. Cela ne se limite pas aux mots, peut être composé de plusieurs "phrases" voire même de plusieurs "expressions". Les expressions sont des éléments de la [nuée](nuee.md) et possèdent donc différents niveaux comme pour le [plumage](plumage.md).

## Les oiseaux

La grammaire ikienne désigne les différentes structures permettant de formaliser des syntaxes comme étant des oiseaux.

Dans le cas du français, par exemple, un grammairien ikien noterait à minima les trois oiseaux suivants :

 * **Oiseau affirmatif** : <ins>O F C</ins> (*SVO*)
 * **Oiseau interrogatif** : <ins>F O C</ins> (*VSO*)
 * **Oiseau impératif** : <ins>F C</ins> (*VO*)

```
(O)rigine
(F)ait
(C)ible

(S)ujet
(V)erbe
(O)bjet
```

> En grammaire ikienne, tout comportement **prévisible** est dit **faible** et inversement, tout comportement **imprévisible** est dit **fort**. Aussi, comme aucun de ces oiseaux, au delà de leur rôle, n'apporte de particularité, on parlera d'oiseaux faibles.

On notera l'usage des termes origine, fait et cible. Il s'agit des éléments de l'expression d'après la grammaire ikienne.

<ins>Les éléments de l'expression :</ins>

 * `L'intention` : La raison derrière l'expression. Pourquoi s'être exprimé.
 * `Le fait` : Le fait exprimé.
 * `L'origine` : Ce qui est à l'origine du fait.
 * `La réaction` : La réaction de l'environnement au fait.
 * `La cible` : La cible du fait. Ce qui est impacté ou visé par le fait.
 * `Le contexte` : Le contexte du fait.
 * `Le coeur` : Si le fait et la cible sont confondus, exprime le cumul des deux.


## La synthèse

La synthèse comprend toute modification de l'oiseau part un élément du [plumage](plumage.md). On parle alors de cet élément comme d'un **synthétiseur**.

On retrouve plusieurs types de synthétiseur :

 * **Synthétiseur impur** : Un synthétiseur qui rentre dans les grandes catégories de synthèse.
 * **Synthétiseur pur** : Un sythétiseur dont au moins un comportement ne rentre dans aucune des grandes catégories de synthèse.
 * **Supersynthétiseur** : Un synthétiseur qui comporte plusieurs comportement.

> Un supersynthétiseur peut donc être pur ou impur.

Pour faire la distinction entre ces synthétiseurs, il est donc nécessaire de connaitre les grandes catégories de synthèse.

<ins>Les grandes catégories de synthèse :</ins>

(Pas encore exhaustif, flemme de mettre le reste)

 * `Le présentateur` : Un présentateur apporte un autre élément en se placant avant (*post introducteur*) ou après (*pré introducteur*).
 * `La superprésentation` : Un superprésentateur introduit une expression entière.
 * `Le marquage` : Un marqueur (à ne pas confondre avec une marque) est un affixe.
 * `Le mariage` : Un marieur (à ne pas confondre avec un marien) est synthétiseur qui vient former un groupe en se plaçant entre deux autres éléments.
 * `La liberté` : Un libre est un synthétiseur qui peut se placer n'importe où indépendemment de l'oiseau.
 * `Le couplage` : Un coupleur vient se coupler avec un autre élément, se plaçant avant (*pré coupleur*) ou après (*post coupleur*).
 * `La tolérance` : Suivant le principe de la [tolérance](tolérance.md), le tolérant est un synthétiseur d'une grande catégorie spécifique à la langue.

> On notera qu'un élément introduit peut être une expression.

